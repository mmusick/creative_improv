/******************************************************************
      Author: Michael Musick
       Email: michael@michaelmusick.com


     Project:
        File:

     Version: 1.0
      M&Year: Feb 2016

 Description: QuNeo Vector Object Class
       Notes:

******************************************************************/

QuVectorObject {

	classvar totalInstances = 0;
	var <instanceID, <active = false, <noteArr, <padArr;

    * new {
        ^super.new.init;
    }

	init {
		if( MIDIClient.initialized.not, {
			MIDIClient.init;
			MIDIIn.connectAll;
		});

		instanceID = totalInstances;
		totalInstances = totalInstances + 1;

		padArr = Array.new(16);
		noteArr = Array.fill(52,
			{ |idx| ( velocity: 0, xPos: 64, yPos: 64, pressure: 0, active: false ) }
		);

		this.loadMIDIdefs();

	}

	* totalInstances{
		^totalInstances
	}

	totalInstances{
		^totalInstances
	}

	loadMIDIdefs{

		/*********************************************
		Registers a new noteOn event
		- Create an object
		*********************************************/
		MIDIdef.noteOn(\padPushed, {
			arg velocity, midiNote, chan, src;

			velocity.postln;
			midiNote.postln;
			chan.postln;
			src.postln;

			// Check if there is another object already
			if( padArr.size > 0, {
				// true
			},{
				// false
				"on".postln;
				active = true;
			});

			padArr.add( midiNote );
			noteArr[midiNote] = ( velocity: velocity, xPos: 64, yPos: 64, pressure: velocity, active: true );

		},
		chan: 0
		);

		/*********************************************
		Clean up arrays for noterOff events
		*********************************************/
		MIDIdef.noteOff(\padReleased, {
			arg velocity, midiNote, chan, src;
			var idx;

			// Check if there is another object already
			if( this.active, {
				// true
				idx = padArr.indexOf(midiNote);
				padArr.removeAt(idx);
				noteArr[midiNote].active = false;
			},{
				// false
			});

			if( padArr.size == 0, {
				"off".postln;
				active = false;
			});
		},
		chan: 0
		);

		/*********************************************
		Update CC Info
		*********************************************/
		MIDIdef.cc(\ccUpdate, {
			arg val, ccNum, chan, src;
			var type;

			// determine is this is xPos, yPos, or Pressure
			type = (ccNum / 16).asInteger;
			ccNum = ccNum % 16;

			switch( type,
				//  xPos
				0, {
					noteArr[ccNum].xPos = val;
				},
				// yPos
				1, {
					noteArr[ccNum].yPos = val;
				},
				// pressure
				2, {
					noteArr[ccNum].pressure = val;
				}
			) ;

		},
		chan: 0
		);

	}

}


/*
SS9_superClass {

	classvar totalBirths = 0, vbap = false, vbapSpeakerArray, vbapBuff;
	var birthstamp, <birth_id, position, <active = false;
	var synth, posTask;

	*new { arg angle, distance, elevation;
		^super.new.init( angle, distance, elevation );
	}
	init { arg angle, distance, elevation;
		birth_id = totalBirths;
		totalBirths = totalBirths + 1;

		position = (
			speed: 0.1,
			angle: 0, distance: 1, elevation: 0,
			curAngle: 0, curDistance: 1, curElevation: 0
		);
*/


